# css project 6

This is project about a sign-up form in css and html

## Technology used

- vsc
- css : google font, flexbox
- html

## Results

1.  Mobile 

2.  Desktop


## Authors and acknowledgment

This project is an exercice which come from [Build 10 CSS Projects in 10 days](https://dev.to/coderamrin)

The original git repository is [here](https://github.com/Coderamrin/build-10-css-projects/tree/main/intro-component-with-signup-form-master)

## License

This project is open source.


